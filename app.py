from flask import Flask, request, redirect, flash, url_for, send_file
from werkzeug.utils import secure_filename
from flask import render_template
# from SVM.main import Main
import os
import sys
# import glob
import cv2
import matplotlib.pyplot as plt
import numpy as np
from scipy._lib.six import xrange
import sys
from PIL import Image, ImageFilter
import math
from collections import Counter
import json
from scipy.ndimage import interpolation as inter
from svm import *
import joblib
import sys

svm = Classifier()

app = Flask(__name__)
app.secret_key = 'some_secret'
allowed_file = ["png","jpeg","jpg"]
allowed_file_csv = ["csv"]
upload_folder = "./static/image/"
app.config['UPLOAD_FOLDER_TRAIN'] = upload_folder+"data_latih"
app.config['UPLOAD_FOLDER_UJI'] = upload_folder+"data_uji"
app.config['UPLOAD_FOLDER_CHARACTER'] = upload_folder+"data_uji_karakter"
app.config['MODEL_FOLDER'] = "model"

def allowed(filename):
    allow = set(allowed_file)
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in allow

def allowed_csv(filename):
    allow = set(allowed_file_csv)
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in allow

@app.route("/")
def home():
    return render_template('home.html')

@app.route("/training")
def training():
    return render_template('training.html')

@app.route("/hasil_training", methods=['POST'])
def hasil_training():

    # ====================== Validation ======================
    if 'file' not in request.files:
        flash('No file part')
        return redirect("/training")

    file = request.files['file']

    if file.filename == '':
        flash('No selected file')
        return redirect("/training")

    if file and allowed(file.filename):
        filename = secure_filename(file.filename)
        clean = os.path.join(app.config['UPLOAD_FOLDER_TRAIN'], filename)
        print(clean)
        file.save(clean)
    else:
        flash('Format file tidak didukung')
        return redirect("/training")

    # PROSES DISINI
    data = {}
    kelas = request.form['label1']

    # ====================== Original Image ======================
    image = cv2.imread("./static/image/data_latih/"+filename)


    # ====================== Grayscale Image ======================

    # the shape of the image
    shape_image = image.shape

    # Intialize a new array of zeroes with the same shape
    grayscale_image = np.zeros((image.shape[0], image.shape[1]))

    # 'Human' Average - adapted for human eyes
    def average1(pixel):
        return 0.299*pixel[0] + 0.587*pixel[1] + 0.114*pixel[2]

    # Raw Average
    def average2(pixel):
        return np.average(pixel)

    # Map averages of pixels to the grey image
    for r in range(len(image)):
        for c in range(len(image[r])):
            # Use human average
            grayscale_image[r][c] = average1(image[r][c])

    # saveimage
    cv2.imwrite("./static/image/data_latih/hasil_grayscale.png", grayscale_image)
    filename_gray = "hasil_grayscale.png"
    gray = cv2.imread("./static/image/data_latih/"+filename_gray)
    shape_gray = gray.shape

    # ====================== Threshold Image ======================
    def threshold():
        threshold=128
        row, col = grayscale_image.shape 
        y = np.zeros((row, col))
        for i in range(0,row):
            for j in range(0,col):
                if grayscale_image[i,j] >= threshold:
                    y[i,j] = 255
                else:
                    y[i,j] = 0
        return y
    threshold_image = threshold()

    #saveimage
    plt.imsave("./static/image/data_latih/hasil_threshold.png", np.array(threshold_image), cmap='gray')
    filename_threshold = "hasil_threshold.png"
    thresh = cv2.imread("./static/image/data_latih/"+filename_threshold)
    shape_thresh = thresh.shape

    #==========================Mengubah ke biner========================
    def binerisasi(threshold_image):
        row, col = threshold_image.shape
        y = np.zeros((row, col))
        for i in range(0, row):
            for j in range(0, col):
                if threshold_image[i, j] == 255:
                    y[i, j] = 0
                else:
                    y[i, j] = 1
        return y

    binary_image = binerisasi(threshold_image)

    # saveimage
    plt.imsave("./static/image/data_latih/hasil_binerisasi.png", binary_image, cmap='gray')
    filename_biner = "hasil_binerisasi.png"
    binerlatih = cv2.imread("./static/image/data_latih/"+filename_biner)
    shape_binerlatih = binerlatih.shape

    #==================Zoning Image Centroid Zone (ICZ) 3 Zona==================
    from skimage.measure import regionprops

    #Centroid of image
    labeled_foreground = (binary_image).astype(int)
    properties = regionprops(labeled_foreground,binary_image)
    center_of_mass = properties[0].centroid
    weighted_center_of_mass = properties[0].weighted_centroid

    print("Koordinat Centroid Citra", center_of_mass)
    print("Nilai Centroid i =", center_of_mass[0])
    print("Nilai Centroid j =", center_of_mass[1])

    #Bagi menjadi 3 zona
    isize, jsize = binary_image.shape
    batas_zona_awal = 0
    batas_zona_akhir = round(jsize/3)
    print(batas_zona_akhir)

    arr_zona = []
    centroid_i = []
    centroid_j = []
    arr_fitur = []

    for a in range(3):
        arr_zona.append(round(jsize/3*a))
    print("Batas zona:",arr_zona)
    print("")

    #Hitung jarak piksel yang mempunyai nilai 1 dari centroid
    print("Jarak Piksel Dari Centroid")
    for n in range(3):
        arr_centroid_i = []
        arr_centroid_j = []
        arr_jarak = []
        if n == 2:
            limit = jsize
        else:
            limit = arr_zona[n+1]
        for a in range(arr_zona[n],limit):
            for b in range(jsize):
                if binary_image[a,b] == 1:
                    arr_centroid_i.append(a+1-arr_zona[n])
                    arr_centroid_j.append(b+1)
        print("Array i zona ke-",n+1,arr_centroid_i)
        print("Array j zona ke-",n+1,arr_centroid_j)
        try:
            #hitung centroid x
            centroid_i = center_of_mass[0]
            centroid_j = center_of_mass[1]
            for a in range(len(arr_centroid_i)):
                #hitung jarak pixel ke centroid
                arr_jarak.append(math.sqrt(pow(arr_centroid_i[a] - round(centroid_i),2) + pow(arr_centroid_j[a] - round(centroid_j),2))) 
            arr_jarak_format = [ '%.4f' % elem for elem in arr_jarak ]
            print("Jarak piksel (Nilai d) zona",n+1,arr_jarak_format)
            arr_fitur.append(sum(arr_jarak) / len(arr_jarak))
            print("Total jarak",sum(arr_jarak))
            print("Jumlah piksel bernilai 1 =",len(arr_jarak))
            print("Nilai fitur Zona",n+1,arr_fitur[n])
            print("")
        except ZeroDivisionError:
            pass

    print("")
    print("Koordinat Centroid Citra", center_of_mass)
    print("Nilai Centroid i =", center_of_mass[0])
    print("Nilai Centroid j =", center_of_mass[1])
    print("Nilai fitur Zona 1 =",arr_fitur[0])
    print("Nilai fitur Zona 2 =",arr_fitur[1])
    print("Nilai fitur Zona 3 =",arr_fitur[n])
    arr_fitur_format = [ '%.4f' % elem for elem in arr_fitur ]
    print("Nilai Fitur Zona 1, Zona 2, Zona 3 :",arr_fitur_format)

    # ====================== Vector Image ======================
    a = np.array(binary_image)
    a.flatten()
    vektor=a.flatten().tolist()
    # print("vektor",len(vektor))
    print("baru")

    # ====================== Save Vector Image ======================
    # file = open("./static/image/data_latih/vektor.txt","a")
    # file.write(json.dumps(vektor))
    # file.write("\n")
    # file.close()
    # tinggal tambahin di sini tapi vektornya ga bisa di tambahin

    def ubahData(data1,kelas,vektor):
        data=[]
        label=[]
        if kelas=="uji":
            for d1 in data1:
                data.append(d1[0])
            return tuple(data)
        else:
            for d1 in data1:
                if str(d1[324]) == kelas:
                    label.append(1)
                else:
                    label.append(-1)
                data.append(d1[0:324])
            # data.append(vektor)
            # label.append(1)
            return tuple(data),tuple(label)
        #endif

    #============= SVM =========================
    print("baca latih")
    data_latih = svm.bacaModel("./static/image/data_latih/vektor.txt")
    # yang bagian ini
    # print(data_latih[0][0:324])
    fix_data = ubahData(data_latih,kelas,vektor)
    # print(len(fix_data[0][0]))
    # print(vektor)
    pelatihan = svm.pelatihan(fix_data[0],fix_data[1],1,10**-5,5)
    # print(pelatihan[0])
    # print(pelatihan[1])

    joblib.dump([fix_data[0],fix_data[1],pelatihan],"./static/model/m"+str(kelas))
    # print(pelatihan)

    # ====================== Export ======================

    data['kelas'] = kelas
    
    data['matrix_ori'] = image
    data['image_ori'] = filename
    data['shape_ori'] = shape_image
    
    data['matrix_gray'] = grayscale_image
    data['filename_gray'] = filename_gray
    data['shape_gray'] = shape_gray
    
    data['matrix_threshold'] = threshold_image
    data['filename_threshold'] = filename_threshold
    data['shape_thresh'] = shape_thresh
    
    data['matrix_biner'] = binary_image
    data['filename_biner'] = filename_biner
    data['shape_binerlatih'] = shape_binerlatih
    
    data['koordinat_centroid_citra'] = center_of_mass
    data['nilai_centroid_i'] = center_of_mass[0]
    data['nilai_centroid_j'] = center_of_mass[1]
    data['nilai_fitur_zona1'] = arr_fitur[0]
    data['nilai_fitur_zona2'] = arr_fitur[1]
    data['nilai_fitur_zona3'] = arr_fitur[n]
    data['nilai_fitur_zona123'] = arr_fitur_format

    data['vektor'] = vektor
    data['pelatihan_svm'] = pelatihan

    return render_template('hasil_training.html', data=data, title="Training")

@app.route("/testing")
def testing():
    return render_template('testing.html', title='testing')

@app.route("/testing_karakter", methods=['POST'])
def testing_karakter():

    type_file = request.form['type']

    if type_file == 'image':
        # Validation Form
        if 'file' not in request.files:
            flash('No file part')
            return redirect("/testing")

        file = request.files['file']

        if file.filename == '':
            flash('No selected file')
            return redirect("/testing")

        if file and allowed(file.filename):
            filename = secure_filename(file.filename)
            clean = os.path.join(app.config['UPLOAD_FOLDER_CHARACTER'], filename)
            print(clean)
            file.save(clean)
        else:
            flash('Format file tidak didukung | Gunakan File .jpg / .png / .jpeg')
            return redirect("/testing")

    elif(type_file == 'document'):
        # Validation Form
        if 'file' not in request.files:
            flash('No file part')
            return redirect("/testing")

        file = request.files['file']

        if file.filename == '':
            flash('No selected file')
            return redirect("/testing")

        if file and allowed_csv(file.filename):
            filename = secure_filename(file.filename)
            clean = os.path.join(app.config['UPLOAD_FOLDER_CHARACTER'], filename)
            print(clean)
            file.save(clean)
        else:
            flash('Format file tidak didukung | Gunakan file CSV')
            return redirect("/testing")

    # PROSES DISINI
    data = {}

    # ====================== Original Image ======================
    image = cv2.imread("./static/image/data_uji_karakter/" + filename)

    # ====================== Grayscale Image ======================

    # the shape of the image
    shape_image = image.shape

    # Intialize a new array of zeroes with the same shape
    grayscale_imageujikarakter = np.zeros((image.shape[0], image.shape[1]))

    # 'Human' Average - adapted for human eyes
    def average1(pixel):
        return 0.299 * pixel[0] + 0.587 * pixel[1] + 0.114 * pixel[2]

    # Raw Average
    def average2(pixel):
        return np.average(pixel)

    # Map averages of pixels to the grey image
    for r in range(len(image)):
        for c in range(len(image[r])):
            # Use human average
            grayscale_imageujikarakter[r][c] = average1(image[r][c])

    # saveimage
    cv2.imwrite("./static/image/data_uji_karakter/hasil_grayscale.png", grayscale_imageujikarakter)
    filename_grayujikarakter = "hasil_grayscale.png"
    graykarakter = cv2.imread("./static/image/data_uji_karakter/"+filename_grayujikarakter)
    shape_graykarakter = graykarakter.shape

    # ====================== Threshold Image ======================
    def threshold():
        threshold=128
        row, col = grayscale_imageujikarakter.shape
        y = np.zeros((row, col))
        for i in range(0,row):
            for j in range(0,col):
                if grayscale_imageujikarakter[i,j] >= threshold:
                    y[i,j] = 255
                else:
                    y[i,j] = 0
        return y
    threshold_imagekarakter = threshold()

    #saveimage
    plt.imsave("./static/image/data_uji_karakter/hasil_threshold.png", np.array(threshold_imagekarakter), cmap='gray')
    filename_thresholdkarakter = "hasil_threshold.png"
    threshujikarakter = cv2.imread("./static/image/data_uji_karakter/"+filename_thresholdkarakter)
    shape_threshuji = threshujikarakter.shape

    # ====================== Resize Image ======================
    width = 18
    height = 18
    dim = (width, height)
    resize_imagekarakter = cv2.resize(threshold_imagekarakter, dim, interpolation=cv2.INTER_LINEAR)

    # saveimage
    plt.imsave("./static/image/data_uji_karakter/resizecitrakarakter.png", np.array(resize_imagekarakter), cmap='gray')
    filename_resizekarakter = "resizecitrakarakter.png"

    # ===========================Ubah hasil resize ke threshold========================
    ret, resizethreshkarakter = cv2.threshold(resize_imagekarakter, 128, 255, cv2.THRESH_BINARY)

    # saveimage
    plt.imsave("./static/image/data_uji_karakter/resizethreshkarakter.png", np.array(resizethreshkarakter), cmap='gray')
    filename_resizethreshkarakter = "resizethreshkarakter.png"
    resizethreshold = cv2.imread("./static/image/data_uji_karakter/"+filename_resizethreshkarakter)
    shape_resizethreshold = resizethreshold.shape

    # ==========================Mengubah ke biner========================
    def binerisasi(resizethreshkarakter):
        row, col = resizethreshkarakter.shape
        y = np.zeros((row, col))
        for i in range(0, row):
            for j in range(0, col):
                if resizethreshkarakter[i, j] == 255:
                    y[i, j] = 0
                else:
                    y[i, j] = 1
        return y

    binary_imagekarakter = binerisasi(resizethreshkarakter)

    # saveimage
    plt.imsave("./static/image/data_uji_karakter/hasil_binerisasikarakter.png", binary_imagekarakter, cmap='gray')
    filename_binerkarakter = "hasil_binerisasikarakter.png"
    binerujikarakter = cv2.imread("./static/image/data_uji_karakter/" + filename_binerkarakter)
    shape_binerujikarakter = binerujikarakter.shape

    # ====================== Vector Image ======================
    a = np.array(binary_imagekarakter)
    a.flatten()
    vektor_uji_karakter = a.flatten().tolist()
    # print(vektor)

    label=[0,1,2,3,4,5,6,7,8,9,"a",".","P","e","m","i","n","l","k","s","S","u","t"]
    # labelnya disini

    #================Uji SVM================================
    hasil = []
    for i in label:
        model = joblib.load("./static/model/m"+str(i))
        print(model)
        hasil.append(svm.pengujian(model[2][0],model[2][1],model[1],model[0],vektor_uji_karakter))

    print(hasil)
    index = hasil.index(max(hasil))
    hyperplane = max(hasil)

    data = {}

    # ====================== Export ======================
    data['matrix_ori'] = image
    data['shape_orikarakter'] = shape_image
    data['image_oriujikarakter'] = filename

    data['matrix_grayujikarakter'] = grayscale_imageujikarakter
    data['filename_grayujikarakter'] = filename_grayujikarakter
    data['shape_graykarakter'] = shape_graykarakter
    
    data['matrix_thresholdkarakter'] = threshold_imagekarakter
    data['filename_thresholdkarakter'] = filename_thresholdkarakter
    data['shape_threshkarakter'] = shape_threshuji
    
    data['filename_resizekarakter'] = filename_resizekarakter
    data['matrix_resizethreshkarakter'] = resizethreshkarakter
    data['filename_resizethreshkarakter'] = filename_resizethreshkarakter
    data['shape_resizethreshold'] = shape_resizethreshold
    
    data['matrix_binerkarakter'] = binary_imagekarakter
    data['filename_binerkarakter'] = filename_binerkarakter
    data['shape_binerujikarakter'] = shape_binerujikarakter
    
    data['vektor_uji_karakter'] = vektor_uji_karakter
    data['nilai_hyperplane'] = hasil
    data['index'] = index
    data['label'] = label
    data['hyperplane'] = hyperplane



    return render_template('hasil_testing_karakter.html', title='testing', data=data)

@app.route("/hasil_testing", methods=['POST'])
def hasil_testing():
    # ====================== Validation ======================
    if 'file' not in request.files:
        flash('No file part')
        return redirect("/")

    file = request.files['file']

    if file.filename == '':
        flash('No selected file')
        return redirect("/")

    if file and allowed(file.filename):
        filename = secure_filename(file.filename)
        clean = os.path.join(app.config['UPLOAD_FOLDER_UJI'], filename)
        print(clean)
        file.save(clean)
    else:
        flash('Format file tidak didukung')
        return redirect("/")

    # PROSES DISINI
    data = {}

    # ====================== Original Image ======================
    image = cv2.imread("./static/image/data_uji/"+filename)
    matrix_image_original = image


    # ====================== Grayscale Image ======================

    # the shape of the image
    shape_image = image.shape

    # Intialize a new array of zeroes with the same shape
    grayscale_image = np.zeros((image.shape[0], image.shape[1]))

    # 'Human' Average - adapted for human eyes
    def average1(pixel):
        return 0.299*pixel[0] + 0.587*pixel[1] + 0.114*pixel[2]

    # Raw Average
    def average2(pixel):
        return np.average(pixel)

    # Map averages of pixels to the grey image
    for r in range(len(image)):
        for c in range(len(image[r])):
            # Use human average
            grayscale_image[r][c] = average1(image[r][c])
    
    # saveimage
    plt.imsave("./static/image/data_uji/grayscale_uji.png", np.array(grayscale_image), cmap='gray')
    filename_gray = "grayscale_uji.png"
    gray = cv2.imread("./static/image/data_uji/"+filename_gray)
    shape_gray = gray.shape


    # ====================== Threshold Image ======================
    def threshold():
        threshold=128
        row, col = grayscale_image.shape
        y = np.zeros((row, col))
        for i in range(0,row):
            for j in range(0,col):
                if grayscale_image[i,j] >= threshold:
                    y[i,j] = 255
                else:
                    y[i,j] = 0
        return y
    threshold_image = threshold()

    #saveimage
    plt.imsave("./static/image/data_uji/threshold_uji.png", np.array(threshold_image), cmap='gray')
    filename_threshold = "threshold_uji.png"
    thresh = cv2.imread("./static/image/data_uji/"+filename_threshold)
    shape_thresh = thresh.shape

    # # ====================== SKEW ======================
    # def find_score(arr, angle):
    #     data = inter.rotate(arr, angle, reshape=False, order=0)
    #     hist = np.sum(data, axis=1)
    #     score = np.sum((hist[1:] - hist[:-1]) ** 2)
    #     return hist, score
    #
    # np.set_printoptions(suppress=True,formatter={'float_kind': '{:f}'.format})
    # delta = 1
    # limit = 20
    # angles = np.arange(-limit, limit+delta, delta)
    # scores = []
    #
    # for angle in angles:
    #     hist, score = find_score(threshold_image, angle)
    #     # print (angle,'{0:.10f}'.format(score))
    #     scores.append(score)
    #
    # best_score = max(scores)
    # best_angle = angles[scores.index(best_score)]
    # #print('Best angle: {}'.format(best_angle))
    #
    # # correct skew
    # skew_image = inter.rotate(threshold_image, best_angle, mode="nearest", reshape=False, order=0)
    #
    # #saveimage
    # plt.imsave("./static/image/data_uji/skew_uji.png", np.array(skew_image), cmap='gray')
    # filename_skew = "skew_uji.png"
    # skew = cv2.imread("./static/image/data_uji/"+filename_skew)
    # shape_skew = skew.shape
    # angle_terbaik = best_angle



    # ====================== LINE SEGMENTATION ======================
    arr = np.array(threshold_image)
    projection = np.sum(arr == 0, axis=1)
    np.set_printoptions(threshold=sys.maxsize)
    line = np.flatnonzero(projection)

    lines = []
    member = []
    number = 0
    number_segmenbaris = 0
    for i in line:
        if projection[i] < 4:
            if member and len(member) > 1:
                image = Image.fromarray(arr[member[0]:member[-1]]).convert("L")
                image.save("./static/image/data_uji/segmentasi_baris" + str(number) + ".jpg")
                number += 1
                number_segmenbaris += 1
                lines.append(member)
            member = []
            continue
        if not member:
            member.append(i)
            continue
        if member[-1] != i - 1:
            if len(member) > 1:
                lines.append(member)

            member = [i]
        else:
            member.append(i)

        # last iteration
        if i == line[-1]:
            if member:
                lines.append(member)

    for i in lines:
        if len(i) < 2:
            continue
        image = Image.fromarray(arr[i[0]:i[-1]])
        plt.figure()
        plt.imshow(image)

        i = lines[0]
    segbaris = Image.fromarray(arr[i[0]:i[-1]])
    #plt.imshow(image)

    # saveimage
    plt.imsave("./static/image/data_uji/baris.png", np.array(segbaris), cmap='gray')
    filename_baris= "baris.png"
    baris = cv2.imread("./static/image/data_uji/" + filename_baris)
    shape_baris = baris.shape

    # # ====================== WORD SEGMENTATION ======================
    # WORD SEGMENTATION
    arrW = np.array(segbaris)
    projection = np.sum(arrW == 0, axis=0)
    # print(projection)
    line = np.flatnonzero(projection)
    asli = projection[line[0]:line[-1] + 1]
    asli = np.where(asli == 0)[0]
    lines = []
    member = []
    gaps = math.ceil(arrW.shape[0] * 0.8)
    print(gaps)
    for i in asli:
        if not member:
            member.append(i)
            continue
        if member[-1] != i - 1:
            # drop if len <= 2
            # if len(member) > 0:
            lines.append(member)

            member = [i]
        else:
            member.append(i)

        # last iteration
        if i == asli[-1]:
            # if member and len(member) > 0:
            lines.append(member)

    # print(lines)
    gaps = int(len(max(lines, key=len)) * 0.8)
    print(gaps)
    # print(len(arrW))
    # print(np.transpose(arrW)[357])
    member = []
    lines = []
    for i in line:

        if not member:
            member.append(i)
            continue
        if member[-1] not in range(i - gaps, i):
            # print(i)
            # drop if len <= 2
            if len(member) > 2:
                lines.append(member)
                # image = Image.fromarray(arr[member[0]:member[-1]]).convert("L")
                # image.save("./static/image/data_uji/segmentasi_kata" + str(number) + ".jpg")
                number += 1

            member = [i]
        else:
            member.append(i)

        # last iteration
        if i == line[-1]:
            if member and len(member) > 2:
                lines.append(member)
                # image = Image.fromarray(arr[member[0]:member[-1]]).convert("L")
                # image.save("./static/image/data_uji/segmentasi_kata" + str(number) + ".jpg")
                number += 1

    arrT = np.transpose(arrW)
    for i in lines:
        dat = np.transpose(arrT[i[0]:i[-1]])
        segkata = Image.fromarray(dat)
        plt.figure()
        plt.imshow(segkata)
    i = lines[1]
    # ima = Image.fromarray(np.transpose(arrT[i[0]:i[-1]]))
    segkata = Image.fromarray(arrW[:, i[0]:i[-1]])

    # saveimage
    plt.imsave("./static/image/data_uji/kata.png", np.array(segkata), cmap='gray')
    filename_kata= "kata.png"
    kata = cv2.imread("./static/image/data_uji/" + filename_kata)
    shape_kata = kata.shape


    # ====================== CHARACTER SEGMENTATION ======================
    from IPython.display import Image as NP_IM
    from IPython.display import display
    arrW = np.array(segkata)
    len_avg = 1 / 3 * arrW.shape[0]
    projection = np.sum(arrW == 0, axis=0)

    line = np.flatnonzero(projection)
    lines = []
    member = []

    for i in line:
        if not member:
            member.append(i)
            continue
        if member[-1] != i - 1:
            # print(len(member))
            if len(member) > 0:
                lines.append(member)
                # image = Image.fromarray(arrW[:, member[0]:member[-1]]).convert("L")
                # image.save("./static/image/data_uji/segmentasi_karakter" + str(number) + ".jpg")
                number += 1

            member = [i]
        else:
            member.append(i)

        # last iteration
        if i == line[-1]:
            # print(len(member))
            if member and len(member) > 0:
                lines.append(member)
                # image = Image.fromarray(arrW[:, member[0]:member[-1]]).convert("L")
                # image.save("./static/image/data_uji/segmentasi_karakter" + str(number) + ".jpg")
                number += 1

    for i in lines:
        c = []
        segkarakter = arrW[:, i[0]:i[-1] + 1]
        # dat = arrW[:, test[0][1][1][0]:test[0][1][1][-1]+1]

        #
        projection = np.sum(segkarakter == 0, axis=1)
        line = np.flatnonzero(projection)
        # print(line)
        # print(dat[line[0]:line[-1]].astype("int"))
        segkarakter = Image.fromarray(segkarakter[line[0]:line[-1] + 1])
        plt.figure()

        # saveimage
        plt.imsave("./static/image/data_uji/huruf.png", np.array(segkarakter), cmap='gray')
        filename_huruf = "huruf.png"
        huruf = cv2.imread("./static/image/data_uji/" + filename_huruf)
        shape_huruf = huruf.shape

        # ====================== Resize Image ======================
        width = 18
        height = 18
        dim = (width, height)
        resize_imageuji = cv2.resize(huruf, dim, interpolation=cv2.INTER_LINEAR)

        # saveimage
        plt.imsave("./static/image/data_uji/resize_uji.png", np.array(resize_imageuji), cmap='gray')
        filename_resizeuji = "resize_uji.png"

        # ===========================Ubah hasil resize ke threshold========================
        ret, resizethreshuji = cv2.threshold(resize_imageuji, 128, 255, cv2.THRESH_BINARY)

        # saveimage
        plt.imsave("./static/image/data_uji/resizethresh_uji.png", np.array(resizethreshuji), cmap='gray')
        filename_resizethreshuji = "resizethresh_uji.png"
        resize_thresholduji = cv2.imread("./static/image/data_uji/" + filename_resizethreshuji)
        shape_resizethresholduji = resize_thresholduji.shape

    # # ====================== Binary Image ======================
    # def binerisasi(resizethreshuji):
    #     row, col = resizethreshuji.shape
    #     y = np.zeros((row, col))
    #     for i in range(0, row):
    #         for j in range(0, col):
    #             if resizethreshuji[i, j] == 255:
    #                 y[i, j] = 0
    #             else:
    #                 y[i, j] = 1
    #     return y
    #
    # binary_image = binerisasi(resizethreshuji)
    #
    # # saveimage
    # plt.imsave("./static/image/data_uji/hasil_biner.png", np.array(binary_image), cmap='gray')
    # filename_biner = "hasil_biner.png"
    # biner = cv2.imread("./static/image/data_uji/" +filename_biner)
    # shape_biner = biner.shape

    # ====================== Export ======================
    data['matrix_ori'] = matrix_image_original
    data['image_oriuji'] = filename
    data['shape_oriuji'] = shape_image

    data['matrix_gray_uji'] = grayscale_image
    data['image_gray_uji'] = filename_gray
    data['shape_gray'] = shape_gray

    data['matrix_thresh_uji'] = threshold_image
    data['image_thresh_uji'] = filename_threshold
    data['shape_thresh'] = shape_thresh

    # data['matrix_skew_uji'] = skew_image
    # data['image_skew_uji'] = filename_skew
    # data['shape_skew'] = shape_skew
    
    # data['angle_terbaik'] = angle_terbaik

    data['image_baris'] = filename_baris
    data['shape_baris'] = shape_baris
    data['matrix_segbaris'] = baris
    data['max_segbaris'] = number_segmenbaris
    
    data['image_kata'] = filename_kata
    data['shape_kata'] = shape_kata
    
    data['image_huruf'] = filename_huruf
    data['shape_huruf'] = shape_huruf
    
    data['image_resizeuji'] = filename_resizeuji
    data['shape_resizethresholduji'] = shape_resizethresholduji

    # data['matrix_biner_uji'] = binary_image
    # data['image_biner_uji'] = filename_biner
    # data['shape_biner'] = shape_biner

    # Some Action Here to Process Data
    return render_template('hasil_testing.html', title="hasil_testing", data=data)


@app.route("/uji_akurasi", methods=['POST', 'GET'])
def uji_akurasi():
    if request.method == 'GET':
        return render_template('uji_akurasi.html', title='uji akurasi')
    else:
        # Validation Form
        if 'file' not in request.files:
            flash('No file part')
            return redirect("/uji_akurasi")

        file = request.files['file']

        if file.filename == '':
            flash('No selected file')
            return redirect("/uji_akurasi")

        if file and allowed_csv(file.filename):
            filename = secure_filename(file.filename)
            clean = os.path.join(app.config['UPLOAD_FOLDER_TRAIN'], filename)
            print(clean)
            file.save(clean)
        else:
            flash('Format file tidak didukung | Gunakan file CSV')
            return redirect("/uji_akurasi")
    nilai = 10
    return render_template('uji_akurasi.html', title="hasil_testing", nilai=nilai)



if __name__ == "__main__":
    app.run(debug=True, port=5001)