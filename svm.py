import numpy as np
import copy
import random as rd
from numpy import mat,shape,zeros,multiply
import json
import sys

class Classifier:
    def pelatihan(self, data, kelas, c, tol, konfergen):
        # c = 1
        # tol = **-7
        X = mat(data)
        Y = mat(kelas).T
        m, n = shape(X)
        maxiter = 100
        # konfergen=20
        ndata = len(data)
        # alpha = np.zeros(ndata, dtype=float)
        alpha = mat(zeros((m, 1)))
        b = 0
        iter = 0
        ikonfergen = 0
        while (ikonfergen < konfergen and iter < maxiter):
            print("=====Iterasi=====", iter)
            gantiAlpha = 0
            for i in range(m):
                print("data ke-", i)
                fx1 = float(multiply(alpha, Y).T * (X * X[i, :].T)) + b
                E1 = fx1 - float(Y[i])
                print("E1")
                # print('nilai e1 ',E1)
                if ((Y[i] * E1 < -tol) and (alpha[i] < c)) or ((Y[i] * E1 > tol) and (alpha[i] > 0)):
                    j = i
                    while (j == i):
                        j = rd.randint(0, ndata - 1)
                    fx2 = float(multiply(alpha, Y).T * (X * X[j, :].T)) + b
                    E2 = fx2 - float(Y[j])
                    print("E2")
                    alpiLama = alpha[i].copy()
                    alpjLama = alpha[j].copy()
                    if Y[i] == Y[j]:
                        L = max(0, alpiLama + alpjLama - c)
                        H = min(c, alpiLama + alpjLama)
                    else:
                        L = max(0, alpjLama - alpiLama)
                        H = min(c, c + alpjLama - alpiLama)
                    if L == H:
                        continue
                    eta = 2.0 * X[i, :] * X[j, :].T - X[i, :] * X[i, :].T - X[j, :] * X[j, :].T
                    print("2")
                    if eta >= 0:
                        continue
                    alpha[j] -= Y[j] * (E1 - E2) / eta
                    alpha[j] = self.clipAlphaJ(alpha[j], H, L)
                    if (abs(alpha[j] - alpjLama) < 0.00001):
                        continue

                    alpha[i] += Y[j] * Y[i] * (alpjLama - alpha[j])

                    b1 = b - E1 - Y[i] * (alpha[i] - alpiLama) * X[i, :] * X[i, :].T - Y[j] * (alpha[j] - alpjLama) * X[
                                                                                                                      i,
                                                                                                                      :] * X[
                                                                                                                           j,
                                                                                                                           :].T
                    b2 = b - E2 - Y[i] * (alpha[i] - alpiLama) * X[i, :] * X[j, :].T - Y[j] * (alpha[j] - alpjLama) * X[
                                                                                                                      j,
                                                                                                                      :] * X[
                                                                                                                           j,
                                                                                                                           :].T
                    print("3")
                    if (0 < alpha[i]) and (c > alpha[i]):
                        b = copy.deepcopy(b1)
                    elif (0 < alpha[j]) and (c > alpha[j]):
                        b = copy.deepcopy(b2)
                    else:
                        b = 0.5 * (b1 + b2)
                    gantiAlpha += 1
                # endif
            # endfor
            if gantiAlpha == 0:
                ikonfergen += 1
            else:
                ikonfergen = 0
            iter = iter + 1
        # endwhile
        # print(self.__alpha)
        return alpha, b

    # def pelatihan(self,data, kelas, c, tol, konfergen):
    #     # c = 1
    #     # tol = **-7
    #     X = mat(data)
    #     Y = mat(kelas).T
    #     m, n = shape(X)
    #     maxiter = 100
    #     # konfergen=20

    #     ndata = len(data)
    #     # alpha = np.zeros(ndata, dtype=float)
    #     alpha = mat(zeros((m, 1)))
    #     b = 0
    #     iter = 0
    #     ikonfergen = 0
    #     # while (ikonfergen < konfergen and iter < maxiter):
    #     print("=====Iterasi=====", iter)
    #     # gantiAlpha = 0
    #     for i in range(m):
    #         print("data ke-", i)
    #         ikonfergen = 0
    #         while(ikonfergen < konfergen):
    #             gantiAlpha = 0
    #             fx1 = float(multiply(alpha, Y).T * (X * X[i, :].T)) + b
    #             E1 = fx1 - float(Y[i])
    #             print("E1")
    #             # print('nilai e1 ',E1)
    #             if ((Y[i] * E1 < -tol) and (alpha[i] < c)) or ((Y[i] * E1 > tol) and (alpha[i] > 0)):
    #                 j = i
    #                 while (j == i):
    #                     j = rd.randint(0, ndata - 1)
    #                 fx2 = float(multiply(alpha, Y).T * (X * X[j,:].T)) + b
    #                 E2 = fx2 - float(Y[j])
    #                 print("E2")
    #                 alpiLama = alpha[i].copy()
    #                 alpjLama = alpha[j].copy()
    #                 if Y[i] == Y[j]:
    #                     L = max(0, alpiLama + alpjLama - c)
    #                     H = min(c, alpiLama + alpjLama)
    #                 else:
    #                     L = max(0, alpjLama - alpiLama)
    #                     H = min(c, c + alpjLama - alpiLama)
    #                 if L == H:
    #                     continue
    #                 eta = 2.0 * X[i,:] * X[j,:].T - X[i,:] * X[i,:].T - X[j,:] * X[j,:].T
    #                 print("2")
    #                 if eta >= 0:
    #                     continue
    #                 alpha[j] -= Y[j] * (E1 - E2) / eta
    #                 alpha[j] = self.clipAlphaJ(alpha[j], H, L)
    #                 if (abs(alpha[j] - alpjLama) < 0.00001):
    #                     continue

    #                 alpha[i] += Y[j] * Y[i] * (alpjLama - alpha[j])

    #                 b1 = b-E1-Y[i]*(alpha[i] - alpiLama) * X[i, :] * X[i, :].T - Y[j] * (alpha[j] - alpjLama) * X[i,:] * X[j,:].T
    #                 b2 = b-E2-Y[i]*(alpha[i] - alpiLama) * X[i, :] * X[j, :].T - Y[j] * (alpha[j] - alpjLama) * X[j,:] * X[j,:].T
    #                 print("3")
    #                 if (0 < alpha[i]) and (c > alpha[i]):
    #                     b = copy.deepcopy(b1)
    #                 elif (0 < alpha[j]) and (c > alpha[j]):
    #                     b = copy.deepcopy(b2)
    #                 else:
    #                     b = 0.5 * (b1 + b2)
    #                 gantiAlpha += 1
    #             # endif
    #             if gantiAlpha == 0:
    #                 ikonfergen += 1
    #             else:
    #                 ikonfergen = 0
    #         # endwhile
    #     # endfor
    #     # if gantiAlpha == 0:
    #     #     ikonfergen += 1
    #     # else:
    #     #     ikonfergen = 0
    #     iter = iter + 1
    #     # endwhile
    #     # print(self.__alpha)
    #     return alpha, b, iter

    def clipAlphaJ(self, aj, H, L):
        if aj > H:
            aj = H
        if L > aj:
            aj = L
        return aj

    def margin(self, data):
        f = copy.deepcopy(self.__b)
        for i in range(self.__ndata):
            f += float(self.__alpha[i] * self.__kelas[i] * self.linierKernel(data, self.__data[i]))
        return f

    def pengujian(self, alpha, b, label, data, vektor):
        X = mat(data)
        Y = mat(label).T
        V = mat(vektor)
        # hasil = []
        # for i in range(len(vektor)):
        # print(multiply(alpha, Y))
        # sys.exit()
        # fx = (X * V[0, :].T) + b
        # print(len(data[0]))
        # print(len(vektor))
        fx = float(multiply(alpha, Y).T * (X * V[0, :].T)) + b
        # fx1 = self.f(alpha, label, b, data, vektor[i])
        # sys.exit()
        # hasil.append(fx)
        return fx

    def f(self, alpha, kelas, b, data, vektor):
        f = b
        # print(kelas[0])
        for i in range(len(data)):
            f += float(alpha[i] * kelas[i] * self.linierKernel(data[i], vektor))
        return f

    def pengujian1(self, alpha, b, kelas, data, uji):
        # n = copy.deepcopy(len(uji))
        ini = []
        for i in range(len(uji)):
            hasil = self.f(alpha, kelas, b, data, uji[i])
            print(hasil)
            ini.append(hasil)
        return ini

    def linierKernel(self, v1, v2):
        s = 0
        for k in range(len(v1)):
            s += v1[k] * v2[k]
        return s

    def saveModel(self, nama_file, alpha, b, iter):
        file = open(nama_file, "w")
        file.write(json.dumps(alpha.tolist()))
        file.write("\n")
        file.write(json.dumps(b.tolist()))
        file.close()

    def bacaModel(self, filenya):
        isi = []
        file = open(filenya, "r")
        ct = file.read().splitlines()
        for idx,i in enumerate(ct):
            isi.append(json.loads(i))
        file.close()
        # sys.exit()
        return isi