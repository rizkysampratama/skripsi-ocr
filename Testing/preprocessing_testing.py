import cv2
import matplotlib.pyplot as plt
import numpy as np
from scipy._lib.six import xrange

image = cv2.imread("tes.png")
new_image = cv2.resize(image,(400, 400))
greyscale_image = cv2.cvtColor(new_image, cv2.COLOR_RGB2GRAY)

thresh = cv2.threshold(greyscale_image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

#deteksi tepi
edges = cv2.Canny(thresh,100,200)

#Skew Correction
coords = np.column_stack(np.where(edges > 0))
angle = cv2.minAreaRect(coords)[-1]
if angle < -45:
    angle = -(90 + angle)
else:
    angle = -angle
# rotate the image to deskew it
(h, w) = edges.shape[:2]
center = (w // 2, h // 2)
M = cv2.getRotationMatrix2D(center, angle, 1.0)
rotated = cv2.warpAffine(edges, M, (w, h),
	flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
# draw the correction angle on the image so we can validate it
cv2.putText(rotated, "Angle: {:.2f} degrees".format(angle),
            (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

# show the output image
print("[INFO] angle: {:.3f}".format(angle))
plt.subplot(2,2,1),plt.imshow(greyscale_image,cmap = 'gray')
plt.title('Grayscale Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,2),plt.imshow(thresh,cmap = 'gray')
plt.title('OTSU Binarization Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(rotated,cmap = 'gray')
plt.title('Skew Correction Image'), plt.xticks([]), plt.yticks([])

plt.show()



