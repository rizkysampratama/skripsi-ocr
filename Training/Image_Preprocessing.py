import cv2
import matplotlib.pyplot as plt

image = cv2.imread("A.png")
new_image = cv2.resize(image,(400, 400))
greyscale_image = cv2.cvtColor(new_image, cv2.COLOR_RGB2GRAY)

thresh = cv2.threshold(greyscale_image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

#deteksi tepi
edges = cv2.Canny(thresh,100,200)

#plt.subplot(2,2,1),plt.imshow(image,cmap = 'gray')
#plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,1),plt.imshow(new_image,cmap = 'gray')
plt.title('Resize Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,2),plt.imshow(greyscale_image,cmap = 'gray')
plt.title('Grayscale Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(thresh,cmap = 'gray')
plt.title('OTSU Binarization Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()


